#!/bin/sh
for filename in $(ls -1 | grep proto)
do
protoc -I. --python_out=./python --grpc_python_out=./python $filename
protoc -I. --js_out=import_style=commonjs:./js --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./js $filename
done